# Astro-UB
# Astro wala
#LoverBoyXD
import asyncio
import os
import requests
import aiohttp
import cv2
import numpy as np
from PIL import Image
from telegraph import upload_file as upf
from telethon.errors.rpcerrorlist import (
    ChatSendMediaForbiddenError,
    MessageDeleteForbiddenError,
) 
from astro.helps.defines import download_file

from astro import CMD_HELP

@astro.on(admin_cmd(pattern="sketch$"))
async def sketch(e):
    ureply = await e.get_reply_message()
    xx = await eor(e, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to image Mr. Pro")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("oki...PProcessing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("__processing__")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    img = cv2.imread(file)
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    inverted_gray_image = 255 - gray_image
    blurred_img = cv2.GaussianBlur(inverted_gray_image, (21, 21), 0)
    inverted_blurred_img = 255 - blurred_img
    pencil_sketch_IMG = cv2.divide(gray_image, inverted_blurred_img, scale=256.0)
    cv2.imwrite("astro.png", pencil_sketch_IMG)
    await e.reply(file="astro.png")
    await xx.delete()
    os.remove(file)
    os.remove("astro.png")


@astro.on(admin_cmd(pattern="grey$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    if not (ureply and (ureply.media)):
        await eor(event, "Reply to Photo")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        xx = await eor(event, "Oki.. Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        xx = await eor(event, "Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    lover = cv2.cvtColor(ult, cv2.COLOR_BGR2GRAY)
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.jpg")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="blur$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    if not (ureply and (ureply.media)):
        await eor(event, "reply to photo")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        xx = await eor(event, "processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        xx = await eor(event, "Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    lover = cv2.GaussianBlur(ult, (35, 35), 0)
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    for i in ["astro.png", "astro.jpg", ultt]:
        if os.path.exists(i):
            os.remove(i)


@astro.on(admin_cmd(pattern="negative$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    xx = await eor(event, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to Photo Mr.Pro🤦")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("Oki... Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    lover = cv2.bitwise_not(ult)
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.jpg")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="mirror$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    xx = await eor(event, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to Photo Mr.Pro🤦")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("Oki... Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    ish = cv2.flip(ult, 1)
    lover = cv2.hconcat([ult, ish])
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.jpg")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="flip$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    xx = await eor(event, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to Photo Mr.Pro🤦")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("Oki... Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    trn = cv2.flip(ult, 1)
    ish = cv2.rotate(trn, cv2.ROTATE_180)
    lover = cv2.vconcat([ult, ish])
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.jpg")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="quad$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    xx = await eor(event, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to Photo Mr.Pro🤦")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("Oki... Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    roid = cv2.flip(ult, 1)
    mici = cv2.hconcat([ult, roid])
    fr = cv2.flip(mici, 1)
    trn = cv2.rotate(fr, cv2.ROTATE_180)
    lover = cv2.vconcat([mici, trn])
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.jpg")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="toon$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    xx = await eor(event, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to Photo Mr.Pro🤦")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("Oki... Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    height, width, channels = ult.shape
    samples = np.zeros([height * width, 3], dtype=np.float32)
    count = 0
    for x in range(height):
        for y in range(width):
            samples[count] = ult[x][y]
            count += 1
    compactness, labels, centers = cv2.kmeans(
        samples,
        12,
        None,
        (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10000, 0.0001),
        5,
        cv2.KMEANS_PP_CENTERS,
    )
    centers = np.uint8(centers)
    ish = centers[labels.flatten()]
    lover = ish.reshape(ult.shape)
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.jpg")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="danger$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    xx = await eor(event, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to Photo Mr.Pro🤦")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("Oki... Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    ult = cv2.imread(file)
    dan = cv2.cvtColor(ult, cv2.COLOR_BGR2RGB)
    lover = cv2.cvtColor(dan, cv2.COLOR_HSV2BGR)
    cv2.imwrite("astro.jpg", lover)
    await event.client.send_file(
        event.chat_id,
        "astro.jpg",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.jpg")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="csample (.*)"))
async def sampl(ult):
    color = ult.pattern_match.group(1)
    if color:
        img = Image.new("RGB", (200, 100), f"{color}")
        img.save("csample.png")
        try:
            try:
                await ult.delete()
                await ult.client.send_message(
                    ult.chat_id, f"Colour Sample for `{color}` !", file="csample.png"
                )
            except MessageDeleteForbiddenError:
                await ult.reply(f"Colour Sample for `{color}` !", file="csample.png")
        except ChatSendMediaForbiddenError:
            await eor(ult, "Umm! Sending Media is disabled here!")

    else:
        await eor(ult, "Wrong Color Name/Hex Code specified!")


@astro.on(admin_cmd(pattern="blue$"))
async def ultd(event):
    ureply = await event.get_reply_message()
    xx = await eor(event, "`...`")
    if not (ureply and (ureply.media)):
        await xx.edit("Reply to Photo Mr.Pro🤦")
        return
    ultt = await ureply.download_media()
    if ultt.endswith(".tgs"):
        await xx.edit("Oki... Processing")
        cmd = ["lottie_convert.py", ultt, "astro.png"]
        file = "astro.png"
        process = await asyncio.create_subprocess_exec(
            *cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        stdout, stderr = await process.communicate()
        stderr.decode().strip()
        stdout.decode().strip()
    else:
        await xx.edit("Processing")
        img = cv2.VideoCapture(ultt)
        heh, lol = img.read()
        cv2.imwrite("astro.png", lol)
        file = "astro.png"
    got = upf(file)
    lnk = f"https://telegra.ph{got[0]}"
    async with aiohttp.ClientSession() as ses:
        async with ses.get(
            f"https://nekobot.xyz/api/imagegen?type=blurpify&image={lnk}"
        ) as out:
            r = await out.json()
    ms = r.get("message")
    if not r["success"]:
        return await xx.edit(ms)
    await download_file(ms["message"], "astro.png")
    img = Image.open("astro.png").convert("RGB")
    img.save("astro.webp", "webp")
    await event.client.send_file(
        event.chat_id,
        "astro.webp",
        force_document=False,
        reply_to=event.reply_to_msg_id,
    )
    await xx.delete()
    os.remove("astro.png")
    os.remove("astro.webp")
    os.remove(ultt)


@astro.on(admin_cmd(pattern="border ?(.*)"))
async def ok(event):
    hm = await event.get_reply_message()
    if not (hm and (hm.photo or hm.sticker)):
        return await eor(event, "`Reply to Sticker or Photo..`")
    col = event.pattern_match.group(1)
    wh = 20
    if not col:
        col = [255, 255, 255]
    else:
        try:
            if ";" in col:
                col_ = col.split(";", maxsplit=1)
                wh = int(col_[1])
                col = col_[0]
            col = [int(col) for col in col.split(",")[:2]]
        except ValueError:
            return await eor(event, "`Not a Valid Input...`")
    okla = await hm.download_media()
    img1 = cv2.imread(okla)
    constant = cv2.copyMakeBorder(img1, wh, wh, wh, wh, cv2.BORDER_CONSTANT, value=col)
    cv2.imwrite("output.png", constant)
    await event.client.send_file(event.chat.id, "output.png")
    os.remove("output.png")
    os.remove(okla)
    await event.delete()


@astro.on(admin_cmd(pattern="pixelator ?(.*)"))
async def pixelator(event):
    reply_message = await event.get_reply_message()
    if not (reply_message and reply_message.photo):
        return await eor(event, "`Reply to a photo`")
    hw = 50
    try:
        hw = int(event.pattern_match.group(1))
    except (ValueError, TypeError):
        pass
    msg = await eor(event, "Processing")
    image = await reply_message.download_media()
    input_ = cv2.imread(image)
    height, width = input_.shape[:2]
    w, h = (hw, hw)
    temp = cv2.resize(input_, (w, h), interpolation=cv2.INTER_LINEAR)
    output = cv2.resize(temp, (width, height), interpolation=cv2.INTER_NEAREST)
    cv2.imwrite("output.jpg", output)
    await msg.respond("• Pixelated by [ƛsτʀ๏](https://t.me/Astro_userbot)", file="output.jpg")
    await msg.delete()
    os.remove("output.jpg")
    os.remove(image)
    
CMD_HELP.update({"imagetools": ".border <reply to photo/sticker>`\
    \n\nTo create border around that media..\
    \nEx - `{i}border 12,22,23`\
    \n- `{i}border 12,22,23 ; width (in number)`\
\n\n.grey <reply to any media>`\
    \nTo make it black nd white.\
\n\n.toon <reply to any media>`\
    \nTo make it toon.\
\n\n.danger <reply to any media>`\
    \nTo make it look Danger.\
\n\n.negative <reply to any media>`\
    \nTo make negative image.\
\n\n.blur <reply to any media>`\
    \nTo make it blurry.\
\n\n.quad <reply to any media>`\
    \ncreate a Vortex.\
\n\n.mirror <reply to any media>`\
    \nTo create mirror pic.\
\n\n.flip <reply to any media>`\
    \nTo make it flip.\
\n\n.sketch <reply to any media>`\
    \nTo draw its sketch.\
\n\n.blue <reply to any media>`\
    \njust cool.\
\n\n.csample <color name /color code>`\
   \nexample : `{i}csample red`\
             \n`{i}csample #ffffff`\
\n\npixelator <reply image>`\
    \nCreate a Pixelated Image.."
  
})
