import glob
import os
import random
from astro.utils import admin_cmd
from astro.helps.defines import unsplashsearch
from astro.helps.defines import make_logo
from telethon.tl.types import InputMessagesFilterPhotos
from astro.helps.defines import download_file, mediainfo


@astro.on(admin_cmd(pattern="logo ?(.*)"))
async def logo_gen(event):
    xx = await event.edit("Processing..")
    name = event.pattern_match.group(1)
    if not name:
        await event.edit("Give me Something also Stupid")
    bg_, font_ = None, None
    if event.reply_to_msg_id:
        temp = await event.get_reply_message()
        if temp.media:
            if hasattr(temp.media, "document") and (
                ("font" in temp.file.mime_type)
                or (".ttf" in temp.file.name)
                or (".otf" in temp.file.name)
            ):
                font_ = await temp.download_media("resources/fonts/")
            elif "pic" in mediainfo(temp.media):
                bg_ = await temp.download_media()
    if not bg_:
        if event.client._bot:
            SRCH = ["blur background", "background", "neon lights", "wallpaper"]
            res = await unsplashsearch(random.choice(SRCH), limit=1)
            bg_ = await download_file(res[0], "resources/downloads/logo.png")
        else:
            pics = []
            async for i in event.client.iter_messages(
                "@Astroub_bgs", filter=InputMessagesFilterPhotos
            ):
                pics.append(i)
            id_ = random.choice(pics)
            bg_ = await id_.download_media()

    if not font_:
        fpath_ = glob.glob("resources/fonts/*")
        font_ = random.choice(fpath_)
    if len(name) <= 8:
        strke = 10
    elif len(name) >= 9:
        strke = 5
    else:
        strke = 20
    make_logo(
        bg_,
        name,
        font_,
        fill="white",
        stroke_width=strke,
        stroke_fill="black",
    )
    flnme = "Logo.png"
    await event.edit("`Done!`")
    if os.path.exists(flnme):
        await event.client.send_file(
            event.chat_id,
            file=flnme,
            caption=f"Logo by [ƛsτʀ๏ υsєяъ๏т](https://t.me/Astro_UserBot)",
            force_document=True,
        )
        os.remove(flnme)
        await event.delete()
    if os.path.exists(bg_):
        os.remove(bg_)
