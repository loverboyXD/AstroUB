import io
import sys
import traceback
from os import remove
from pprint import pprint
from astro.utils import admin_cmd
from astro.helps.defines import bash 
from astro import CMD_HELP

@astro.on(admin_cmd(pattern="bash ?(.*)")) 
@astro.on(sudo_cmd(pattern="exec", allow_sudo=True))
async def _(event):
    await eor(event, "__Processing...__")
    try:
        cmd = event.text.split(" ", maxsplit=1)[1]
    except IndexError:
        return await eor(event, "Invalid Syntax")
    reply_to_id = event.message.id
    if event.reply_to_msg_id:
        reply_to_id = event.reply_to_msg_id
    stdout, stderr = await bash(cmd)
    OUT = f"**•⋗ Bᴀsʜ\n\n• COMMAND:**\n`{cmd}` \n\n"
    if stderr:
        OUT += f"**• Eʀʀᴏʀ:** \n`{stderr}`\n\n"
    if stdout:
        _o = stdout.split("\n")
        o = "\n".join(_o)
        OUT += f"**• OUTPUT:**\n`{o}`"
    if not stderr and not stdout:
        OUT += "**• OUTPUT:**\n`Success`"
    if len(OUT) > 4096:
        ultd = OUT.replace("`", "").replace("**", "").replace("__", "")
        with io.BytesIO(str.encode(ultd)) as out_file:
            out_file.name = "bash.txt"
            await event.client.send_file(
                event.chat_id,
                out_file,
                force_document=True,
                thumb="resources/Astro.jpg",
                allow_cache=False,
                caption=f"`{cmd}`" if len(cmd) < 998 else None,
                reply_to=reply_to_id,
            )

            await event.delete()
    else:
        await event.edit(OUT)
        
CMD_HELP.update({"bash": ".bash <code>\nUse - Run a Linux code."})
