from astro.utils import admin_cmd
from astro import CMD_HELP

@borg.on(admin_cmd(pattern="ohk ?(.*)"))
async def oki(event):
      await event.edit("""░█████╗░██╗░░██╗██╗░░██╗
██╔══██╗██║░░██║██║░██╔╝
██║░░██║███████║█████═╝░
██║░░██║██╔══██║██╔═██╗░
╚█████╔╝██║░░██║██║░╚██╗
░╚════╝░╚═╝░░╚═╝╚═╝░░╚═╝
""")

CMD_HELP.update({"ohk": ".ohk - Say ohk to everyone"})