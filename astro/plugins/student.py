from astro.utils import admin_cmd
from astro import CMD_HELP
import asyncio

@astro.on(admin_cmd(pattern="student$"))
async def _(event):
    if event.fwd_from:
        return
    animation_interval = 1
    animation_ttl = range(0, 14)
    await event.edit("STUDENT")
    animation_chars = [
    "Today let me show you\nTough Students Life😢",
    "How a Students Suffers with a  Smile",
    "So Lets Start!",
    "First Wakup..\nBreak a Beautiful Sleep😫",
    "Get Ready..\n",
    "Go to School...\nand Follow Rules Strictly 😑😢",
    "Like**\nIf you are Hungry Don't eat Until Meal Time... Doesn't matter how Much Hungry you are..😢",
    "NO2**\n Never Sleep in class..🥲\nFirst Wakup Early But they Don't care.. if you slept even by Mistake\nGet Punished 😢",
    "Many More...",
    "Come to Home..",
    "Do Fcking 'HomeWork'",
    "What is Profit of HomeWork still i didn't understood😑 if we can learn all from books",
    "HomeWork of every Subject...",
    "Half Day Took School\n3-4H HomeWork",
    "Some How 30min to 1H left for Play",
    "Even This Time also we can't enjoy...",
    "Back to Studies\nBy doing So-called\nRevise😫",
    "This also Take 2-3H",
    "30-20min. left for enjoyment and rest in which you Have to Have Dinner...",
    "Then Sleep and from Tommorow Back to School 😢Do again this Routine",
    "THIS didn't Finishes yet..",
    "Exams are Still To Fck us😫😖",
    "Doesn't matter How Much you Studies",
    "Every One Still say**\nHe don't do Studies",
    "Parents apply Pressure Because of Competition with other Students",
    "They Don't want Our Knowledge",
    "Specially in our Country..\nKnowledge doesn't matter of a Child\nMarks matter😢😖",
    "IF someone got 90% marks He is Everyone's Favourite\nOther Side: if Someone got Passing marks\nEven There Parents Treat Them as a Servent/animal\nNo Value of us",
    "Not a Single one will say...\n'Hey My Boy/Girl Don't worry... you will get better nextTime'\nThey Don't say 'Do as your interest..Just don't do wrong'",
    "They Think He can't do..Never Motivate",
    "Which Leds Overthinking, Wrong decission, Anger, Depression For a Child",
    "This Much Happnes Still A Student Have a Smile :)",
    "Hope This shows you How much a Child Suffers from society, family, exams, works",
    ]
    for i in animation_ttl:

        await asyncio.sleep(animation_interval)
        await event.edit(animation_chars[i % 14])

CMD_HELP.update({"student": ".student\
\nUSE - Shows pain of a Student🥲"})