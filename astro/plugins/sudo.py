# Astro-UB
# © AstroUB

import os
from astro import CMD_HELP
from astro.utils import admin_cmd
import heroku3
from telethon.tl.functions.users import GetFullUserRequest

Heroku = heroku3.from_key(Config.HEROKU_API_KEY)
heroku_api = "https://api.heroku.com"
sudousers = os.environ.get("SUDO_USERS", None)


@astro.on(admin_cmd(pattern="sudolist"))
async def sudo(event):
    sudo = "True" if Config.SUDO_USERS else "False"
    users = os.environ.get("SUDO_USERS", None)
    if sudo == "True":
        await eor(event, f"**Astro**\nSudo - `Enabled`\nSudo user(s) - `{users}`")
    else:
        await eor(event, f"**astro**\nSudo - `Disabled`")


@astro.on(admin_cmd(pattern="sdhndlr"))
async def handler(event):
    hndlr = Config.CMD_HNDLR
    if hndlr == r"\.":
        x = "."
    else:
        x = Config.CMD_HNDLR

    sudohndlr = Config.SUDO_HNDLR
    await eor(event, f"Command Handler - {x}\nSudo Handler - {sudohndlr}")


@astro.on(admin_cmd(pattern="addsudo(?: |$)"))
async def tb(event):
    ok = await eor(event, "Adding user as a sudo...")
    astro = "SUDO_USERS"
    if Config.HEROKU_APP_NAME is not None:
        app = Heroku.app(Config.HEROKU_APP_NAME)
    else:
        await ok.edit("`[HEROKU]:" "\nPlease setup your` **HEROKU_APP_NAME**")
        return
    heroku_Config = app.config()
    if event is None:
        return
    try:
        target = await get_user(event)
    except Exception:
        await ok.edit(f"Reply to a user.")
    if sudousers:
        newsudo = f"{sudousers} {target}"
    else:
        newsudo = f"{target}"
    await ok.edit(f"Added `{target}` as a sudo user. Restarting.. Give me a minute...")
    heroku_Config[astro] = newsudo


async def get_user(event):
    if event.reply_to_msg_id:
        previous_message = await event.get_reply_message()
        if previous_message.forward:
            replied_user = await event.client(
                GetFullUserRequest(previous_message.forward.sender_id)
            )
        else:
            replied_user = await event.client(
                GetFullUserRequest(previous_message.sender_id)
            )
    target = replied_user.user.id
    return target

@astro.on(admin_cmd(pattern="delsudo ?(.*)"))
async def _(ult):
    inputs = ult.pattern_match.group(1)
    if ult.reply_to_msg_id:
        replied_to = await ult.get_reply_message()
        id = replied_to.sender_id
        name = get_display_name(replied_to.sender)
    elif inputs:
        id = await get_user_id(inputs)
        try:
            name = (await ult.client.get_entity(int(id))).first_name
        except BaseException:
            name = ""
    elif ult.is_private:
        id = ult.chat_id
        name = get_display_name(ult.chat)
    else:
        return await eor(ult, "Reply to a msg or add it's id/username..!", time=5)
    if not is_sudo(id):
        if name != "":
            mmm = f"[{name}](tg://user?id={id}) `wasn't a SUDO User ...`"
        else:
            mmm = f"`{id} wasn't a SUDO User...`"
    elif del_sudo(id):
        if name != "":
            mmm = f"**Removed [{name}](tg://user?id={id}) from SUDO User(s)**"
        else:
            mmm = f"**Removed **`{id}`** from SUDO User(s)**"
    else:
        mmm = "`SEEMS LIKE THIS FUNCTION CHOOSE TO BREAK ITSELF`"
    await eor(ult, mmm, time=5)
    
    
CMD_HELP.update({"sudo": ".sudolist\
use - Get List of Users.\
\n.addsudo use - Add User as sudo user\
\n.sdhndlr use - Change hndlr of sudo user\
\n.delsudo use - delete User from sudo users"
})