import certifi
import os
import time
from astro import LOGS, CMD_HELP
import cv2
import ssl
from PIL import Image 
from astro.helps.asyn import async_searcher
from telegraph import upload_file as uf

from astro.helps.defines import bash, downloader, uploader, convert

opn = []
async def get_paste(data: str, extension: str = "txt"):
    ssl_context = ssl.create_default_context(cafile=certifi.where())
    json = {"content": data, "extension": extension}
    key = await async_searcher(
        url="https://spaceb.in/api/v1/documents/",
        json=json,
        ssl=ssl_context,
        post=True,
        re_json=True,
    )
    try:
        return True, key["payload"]["id"]
    except KeyError:
        if "the length must be between 2 and 400000." in key["error"]:
            return await get_paste(data[-400000:], extension=extension)
        return False, key["error"]
    except Exception as e:
        LOGS.info(e)
        return None, str(e)
        
@astro.on(admin_cmd(pattern="rename( (.*)|$)"))
async def imak(event):
    reply = await event.get_reply_message()
    t = time.time()
    if not reply:
        return await event.eor("Reply to File..")
    inp = event.pattern_match.group(1).strip()
    if not inp:
        return await event.eor("Give the input name")
    xx = await event.eor("Processing...")
    if reply.media:
        if hasattr(reply.media, "document"):
            file = reply.media.document
            image = await downloader(
                reply.file.name or str(time.time()),
                reply.media.document,
                xx,
                t,
                "Download."
            )
            file = image.name
        else:
            file = await event.client.download_media(reply.media)
    if os.path.exists(inp):
        os.remove(inp)
    await bash(f'mv """{file}""" """{inp}"""')
    if not os.path.exists(inp) or os.path.exists(inp) and not os.path.getsize(inp):
        os.rename(file, inp)
    k = time.time()
    xxx = await uploader(inp, inp, k, xx, "Uploaded...")
    await event.reply(
        f"`{xxx.name}`",
        file=xxx,
        force_document=True,
        thumb="resources/Astro.jpg",
    )
    os.remove(inp)
    await xx.delete()


conv_keys = {
    "img": "png",
    "sticker": "webp",
    "webp": "webp",
    "image": "png",
    "webm": "webm",
    "gif": "gif",
}


@astro.on(admin_cmd(pattern="convt( (.*)|$)"))
async def uconverter(event):
    xx = await event.eor("Processing....")
    a = await event.get_reply_message()
    input_ = event.pattern_match.group(1).strip()
    b = await a.download_media("resources/downloads/")
    if not b and (a.document and a.document.thumbs):
        b = await a.download_media(thumb=-1)
    if not b:
        return await xx.edit("Error in download")
    try:
        convert = conv_keys[input_]
    except KeyError:
        return await xx.edit("Please have correct input from following:\n{}, {}, {}, {}".format("gif/img/sticker/webm"))
    file = await convert(b, outname="astro", convert_to=convert)
    if file:
        await event.client.send_file(
            event.chat_id, file, reply_to=event.reply_to_msg_id or event.id
        )
        os.remove(file)
    await xx.delete()


@astro.on(admin_cmd(pattern="repack( (.*)|$)"))
async def _(event):
    input_str = event.pattern_match.group(1).strip()
    if not (input_str and event.is_reply):
        return await event.eor("Please Reply with File with Extension")
    xx = await event.eor("Processing...")
    a = await event.get_reply_message()
    if not a.message:
        return await xx.edit("Please reply to file..")
    with open(input_str, "w") as b:
        b.write(str(a.message))
    await xx.edit(f"**Packing into** `{input_str}`")
    await event.reply(file=input_str, thumb="resources/Astro.jpg")
    await xx.delete()
    os.remove(input_str)


@astro.on(admin_cmd(pattern="open( (.*)|$)"))
async def _(event):
    a = await event.get_reply_message()
    b = event.pattern_match.group(1).strip()
    if not ((a and a.media) or (b and os.path.exists(b))):
        return await event.eor("Please Reply to File")
    xx = await event.eor("Processing...")
    rem = None
    if not b:
        b = await a.download_media()
        rem = True
    try:
        with open(b) as c:
            d = c.read()
    except UnicodeDecodeError:
        return await xx.eor("Wrong Format")
    try:
        await xx.edit(f"```{d}```")
    except BaseException:
        what, key = await get_paste(d)
        await xx.edit(
            f"**MESSAGE EXCEEDS TELEGRAM LIMITS**\n\nSo Pasted It On [SPACEBIN](https://spaceb.in/{key})"
        )
    if rem:
        os.remove(b)
        
CMD_HELP.update({"doctools": ".convt <gif/img/sticker/webm>`\
\nUSE - Reply to media to convert it into gif / image / webm / normal sticker.\
\n\n.repack <filename.ext>`\
\nUSE - Reply to a text msg to save it in a file.\
\n\nopen <Reply to file>\
\nUSE - Reply to a file to reveal it's text.\
\n\nrename <file name with extension>`\
\nUSE - Rename the file"})
