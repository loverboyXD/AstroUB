
#MADE BY Sahil_Beneath
#bug fixed by @Mellowxd
# Using AstroUB

import asyncio
import os
from astro.utils import admin_cmd
from astro import CMD_HELP

M = ("┈┈╱▔▔▔▔▔▔▔▔▔▔▔▏\n" 
     "┈╱╭▏╮╭┻┻╮╭┻┻╮╭▏ \n"
     "▕╮╰▏╯┃╭╮┃┃╭╮┃╰▏ \n"
     "▕╯┈▏┈┗┻┻┛┗┻┻┻╮▏ \n"
     "▕╭╮▏╮┈┈┈┈┏━━━╯▏\n" 
     "▕╰╯▏╯╰┳┳┳┳┳┳╯╭▏ \n"
     "▕┈╭▏╭╮┃┗┛┗┛┃┈╰▏ \n"
     "▕┈╰▏╰╯╰━━━━╯┈┈▏\n")

@astro.on(admin_cmd(pattern=r"spong"))
async def kek(kek):
  await kek.edit(M)
 
  D = ("╭━┳━╭━╭━╮╮\n"
       "┃┈┈┈┣▅╋▅┫┃\n"
       "┃┈┃┈╰━╰━━━━━━╮\n"
       "╰┳╯┈┈┈┈┈┈┈┈┈◢▉◣\n"
       "╲┃┈┈┈┈┈┈┈┈┈┈▉▉▉\n"
       "╲┃┈┈┈┈┈┈┈┈┈┈◥▉◤\n"
       "╲┃┈┈┈┈╭━┳━━━━╯\n"
       "╲┣━━━━━━┫\n")
       
@astro.on(admin_cmd(pattern=r"kutta"))
async def dog(dog):
  await dog.edit(D)
  P = ("┈┏━╮╭━┓┈╭━━━━╮\n"
       "┈┃┏┗┛┓┃╭┫ⓞⓘⓝⓚ┃\n"
       "┈╰┓▋▋┏╯╯╰━━━━╯\n"
       "╭━┻╮╲┗━━━━╮╭╮┈\n"
       "┃▎▎┃╲╲╲╲╲╲┣━╯┈\n"
       "╰━┳┻▅╯╲╲╲╲┃┈┈┈\n"
       "┈┈╰━┳┓┏┳┓┏╯┈┈┈\n"
       "┈┈┈┈┗┻┛┗┻┛┈┈┈┈\n")
       
   
      
  
F =(" ╱▏┈┈┈┈┈┈▕╲▕╲┈┈┈\n"
     "▏▏┈┈┈┈┈┈▕▏▔▔╲┈┈\n"
     "▏╲┈┈┈┈┈┈╱┈▔┈▔╲┈\n"
    "╲▏▔▔▔▔▔▔╯╯╰┳━━▀\n"
    "┈▏╯╯╯╯╯╯╯╯╱┃┈┈┈\n"
    "┈┃┏┳┳━━━┫┣┳┃┈┈┈\n"
    "┈┃┃┃┃┈┈┈┃┃┃┃┈┈┈\n"
    "┈┗┛┗┛┈┈┈┗┛┗┛┈┈┈\n")
    
E =("┈┈┈┈╱▔▔▔▔▔╲┈╱▔╲\n"
    "┈┈┈┈▏┈┈▏╭╮▕┈▏╳▕\n"
    "┈┈┈┈▏┈┈▏┈┈▕┈╲▂╱\n"
    "┈╱▔▔╲▂╱╲▂▂┈╲▂▏▏\n"
    "╭▏┈┈┈┈┈┈┈▏╲▂▂╱┈\n"
    "┃▏┈┈┈┈▏┈┈▏┈┈┈┈┈\n"
    "╯▏┈╲╱▔╲▅▅▏┈┈┈┈\n"
    "┈╲▅▅▏▕▔▔▔▔▏┈┈┈┈\n")
    
H = ("┈┈┈╭━━━━━╮┈┈┈┈┈\n"
     "┈┈┈┃┊┊┊┊┊┃┈┈┈┈┈\n"
     "┈┈┈┃┊┊╭━╮┻╮┈┈┈┈\n"
     "┈┈┈╱╲┊┃▋┃▋┃┈┈┈┈\n"
      "┈┈╭┻┊┊╰━┻━╮┈┈┈┈\n"
       "┈┈╰┳┊╭━━━┳╯┈┈┈┈\n"
       "┈┈┈┃┊┃╰━━┫┈HOMER\n"
       "┈┈┈┈┈┈┏━┓┈┈┈┈┈┈\n")
      
    
    
@astro.on(admin_cmd(pattern=r"fox"))
async def fox(fox):
  await fox.edit(F)
  
@astro.on(admin_cmd(pattern=r"elephant"))
async def elephant(elephant):
  await elephant.edit(E)

@astro.on(admin_cmd(pattern=r"homer"))
async def homer(homer):
  await homer.edit(H)

@astro.on(admin_cmd(pattern=r"pig"))
async def pig(pig):
  await pig.edit(P)
  

CMD_HELP.update({"animals": ".pig\
\nuse - see a pig\
\n.homer\
\nuse - check your self\
\nelephant\
\n use - u know\
\nfox\
\nuse - show a fox\
\nkutta\
\nuse - different kind of Dog\
\nspong\
\nuse - Spong bob"
})