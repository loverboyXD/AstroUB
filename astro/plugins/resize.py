import os 
from PIL import Image
from astro import CMD_HELP 
from astro.utils import admin_cmd
from astro.utils import sudo_cmd 
from astro.config import Config 
from astro.helps.edef import eor

HNDLR = Config.HNDLR 

@astro.on(admin_cmd(pattern="size$"))
async def size(e):
    r = await e.get_reply_message()
    if not (r and r.media):
        return await e.eor("Reply to Media🥲")
    k = await e.eor("Processing....")
    if hasattr(r.media, "document"):
        img = await e.client.download_media(r, thumb=-1)
    else:
        img = await r.download_media()
    im = Image.open(img)
    x, y = im.size
    await k.edit(f"Dimension Of This Image Is\n`{x} x {y}`")
    os.remove(img)


@astro.on(admin_cmd(pattern="resize ?(.*)"))
async def size(e):
    r = await e.get_reply_message()
    if not (r and r.media):
        return await e.eor("Reply to media Mr.Pro🥲💔")
    sz = e.pattern_match.group(1)
    if not sz:
        return await eor(
            f"Give Some Size To Resize, Like `{HNDLR}resize 720 1080` ")
    k = await e.eor("Processing...")
    if hasattr(r.media, "document"):
        img = await e.client.download_media(r, thumb=-1)
    else:
        img = await r.download_media()
    sz = sz.split()
    if len(sz) != 2:
        return await eor(
            k, f"Give Some Size To Resize, Like `{HNDLR}resize 720 1080` ")
    x, y = int(sz[0]), int(sz[1])
    im = Image.open(img)
    ok = im.resize((x, y))
    ok.save(img, format="PNG", optimize=True)
    await e.reply(file=img)
    os.remove(img)
    await k.delete()
    
CMD_HELP.update({"resize": ".size <reply to media>`\
\nTo get size of it.\
\n\n.resize <number> <number>`\
\nTo resize image on x, y axis.\
\neg. `{i}resize 690 960`"
  
})
