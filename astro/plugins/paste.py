import requests
import json
import os

from astro.utils import admin_cmd

spaceb_url = "https://spaceb.in/api/v1/documents/"

def spacebin(data, ext="txt"):
    try:
        request = requests.post(
            spaceb_url, 
            data={
                "content": data.encode("UTF-8"),
                "extension": ext,
            },
        )
    except Exception as ex:
        return f"#Error : {ex}"
    r = request.json()
    key = r.get('payload').get('id')
    if key is not None:
        return {
            "bin": "SpaceBin",
            "link": f"https://spaceb.in/{key}",
            "raw": f"{spaceb_url}{key}/raw",
        }
    else:
        return "#Error : No response."
        
@astro.on(admin_cmd(pattern="paste ?(.*)"))
async def paste(e):
    if e.fwd_from:
        return
    data, ext = '', 'txt' 
    hmm = await eor(e, "__Pasting....__")
    if e.is_reply:
        reply = await e.get_reply_message()
        if reply.media:
            if reply.file.size > 15000000: # 14MB
                return await eor(hmm, "`File too big to paste`")
            try:
                ext = reply.file.ext.replace('.', '')
                _dl = await e.client.download_media(reply)
                with open(_dl, 'r') as f:
                    data = f.read()
                    f.close()
            except Exception as ex:
                return await hmm.edit(f'#Error : `{ex}`')
            finally:
                os.remove(_dl)
        else:
            data = reply.message
    elif args:
        data = args
    else:
        await eor(hmm, "`Reply to a msg/file..`")
        return
    out = spacebin(data, ext)
    
    if isinstance(out, dict):
        c1m = f"<b>Pasted To SpaceBin🛸🌌🔭\nSᴘᴀᴄᴇBɪɴ: <a href='{out['link']}'>{out['bin']}</a>\n"\
        f"Rᴀᴡ: <a href='{out['raw']}'>Raw</a></b>"
        await hmm.edit(c1m, parse_mode="html", link_preview=False)
    else:
        return await eor(hmm, str(out))
