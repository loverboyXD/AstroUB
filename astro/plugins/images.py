import os
import requests
from bs4 import BeautifulSoup as bs
from astro import astro
from PIL import Image
from astro.utils import admin_cmd
from astro.helps.asyn import async_searcher
from astro.google_images_download import googleimagesdownload
from astro.helps.defines import convert
from shutil import rmtree

@astro.on(admin_cmd(pattern="img( (.*)|$)"))
async def goimg(event):
    query = event.pattern_match.group(1).strip()
    if not query:
        return await event.eor("Provide Some Thing my pro MAster💔")
    nn = await event.eor("Processing.......")
    lmt = 5
    if ";" in query:
        try:
            lmt = int(query.split(";")[1])
            query = query.split(";")[0]
        except BaseException:
            pass
    try:
        gi = googleimagesdownload()
        args = {
            "keywords": query,
            "limit": lmt,
            "format": "jpg",
            "output_directory": "./resources/downloads/",
        }
        pth = await gi.download(args)
        ok = pth[0][query]
    except BaseException:
        return await nn.edit("No Reasults Related to {}".format(query))
    await event.reply(file=ok, message=query)
    rmtree(f"./resources/downloads/{query}/")
    await nn.delete()


@astro.on(admin_cmd(pattern="reverse$"))
async def reverse(event):
    reply = await event.get_reply_message()
    if not reply:
        return await event.eor("`Reply to an Image`")
    ult = await event.eor("Proccessing...")
    dl = await reply.download_media()
    file = await convert(dl, convert_to="png")
    img = Image.open(file)
    x, y = img.size
    files = {"encoded_image": (file, open(file, "rb"))}
    grs = requests.post(
        "https://www.google.com/searchbyimage/upload",
        files=files,
        allow_redirects=False,
    )
    loc = grs.headers.get("Location")
    response = await async_searcher(
        loc,
        headers={
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0",
        },
    )
    xx = bs(response, "html.parser")
    div = xx.find_all("div", {"class": "r5a77d"})[0]
    alls = div.find("a")
    link = alls["href"]
    text = alls.text
    await ult.edit(f"`Dimension ~ {x} : {y}`\nSauce ~ [{text}](google.com{link})")
    gi = googleimagesdownload()
    args = {
        "keywords": text,
        "limit": 2,
        "format": "jpg",
        "output_directory": "./resources/downloads/",
    }
    pth = await gi.download(args)
    ok = pth[0][text]
    await event.client.send_file(
        event.chat_id,
        ok,
        album=True,
        caption="Similar Images Realted to Search",
    )
    rmtree(f"./resources/downloads/{text}/")
    os.remove(file)
