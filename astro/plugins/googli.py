

"""

✘ Commands Available -

"""

import io
import os
import random
from emoji import get_emoji_regexp
from astro.utils import admin_cmd
from astro import CMD_HELP

async def hide_inlinebot(borg, bot_name, text, chat_id, reply_to_id, c_lick=0):
    sticcers = await borg.inline_query(bot_name, f"{text}.")
    cat = await sticcers[c_lick].click("me", hide_via=True)
    if cat:
        await borg.send_file(int(chat_id), cat, reply_to=reply_to_id)
        await cat.delete()

async def reply_id(event):
    reply_to_id = None
    if event.reply_to_msg_id:
        reply_to_id = event.reply_to_msg_id
    return reply_to_id

def deEmojify(inputString: str) -> str:
    """Remove emojis and other non-safe characters from string"""
    return get_emoji_regexp().sub("", inputString)

@astro.on(admin_cmd(pattern="googli(?:\s|$)([\s\S]*)"))
async def twt(event):
    "Search in google animation."
    text = event.pattern_match.group(1)
    reply_to_id = await reply_id(event)
    bot_name = "@GooglaxBot"
    if not text:
        if event.is_reply:
            text = (await event.get_reply_message()).message
        else:
            return await edit_delete(
                event, "__What am I supposed to search? Give some text.__"
            )
    text = deEmojify(text)
    await event.delete()
    await hide_inlinebot(event.client, bot_name, text, event.chat_id, reply_to_id)
CMD_HELP.update({"googli": ".googli\
\nUse - type Your word in google search bar"
})