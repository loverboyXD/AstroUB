# Special For Astro-UB 
# ©AstroUB™

from datetime import datetime

from telethon.tl.functions.channels import EditBannedRequest
from telethon.tl.types import ChatBannedRights, MessageEntityMentionName

from astro import CMD_HELP
from astro.utils import admin_cmd
from astro.config import Config

RER = Config.NAME 

FEK = str(RER) if RER else "Astro User✨"


BANNED_RIGHTS = ChatBannedRights(
  until_date = None,
  view_messages = True,
  send_messages = True,
  send_media = True,
  send_stickers = True,
  send_gifs = True,
  send_games = True,
  send_inline = True,
  embed_links = True,
)
UNBAN_RIGHTS = ChatBannedRights(
  until_date = None,
  send_messages = None,
  send_media = None,
  send_stickers = None,
  send_gifs = None,
  send_games = None,
  send_inline = None,
  embed_links = None,
)



@borg.on(admin_cmd(pattern = "(ban|unban) ?(.*)", allow_sudo = True))
async def _(event):
    if event.fwd_from:
      return
      datetime.now()
      to_ban_id = None
      rights = None
      input_cmd = event.pattern_match.group(1)
      if input_cmd == "ban":
        rights = BANNED_RIGHTS
      elif input_cmd == "unban":
        rights = UNBAN_RIGHTS
      input_str = event.pattern_match.group(2)
      reply_msg_id = event.reply_to_msg_id
      if reply_msg_id:
        r_mesg = await event.get_reply_message()
        to_ban_id = r_mesg.from_id
      elif input_str and "all" not in input_str:
        to_ban_id = int(input_str)
      else:
        return False
      try:
        await borg(EditBannedRequest(event.chat_id, to_ban_id, rights))
      except (Exception) as exc:
        await event.edit(str(exc))
      else:
        await event.edit(f" {input_cmd}ned **Successfully by {FEK}")


CMD_HELP.update({"ban": ".ban - use to Ban Someone from group\
\n.unban To unban Someone From group"})