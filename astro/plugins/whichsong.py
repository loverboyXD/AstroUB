import os 
os.system("pip install shazamio")
from os import remove
from shazamio import Shazam
from astro.helps.defines import mediainfo
from astro.utils import admin_cmd
from astro import CMD_HELP 

shazam = Shazam()


@astro.on(admin_cmd(pattern="whichsong$"))
async def song_recog(event):
    reply = await event.get_reply_message()
    if not (reply and mediainfo(reply.media) == "audio"):
        return await eor(event, "Reply to Audio File.")
    await eor(event, "Trying to recognise Audio")
    path_to_song = "./temp/shaazam_cache/unknown.mp3"
    await reply.download_media(path_to_song)
    await event.edit("Yes! got\nFetching details")
    try:
        res = await shazam.recognize_song(path_to_song)
    except Exception as e:
        return await eor(event, str(e), time=10)
    remove(path_to_song)
    try:
        x = res["track"]
        await event.edit("This Song Name is {}".format(x["title"]))
    except KeyError:
        return await eor(event, "Error can't find!")
        
CMD_HELP.update({"whichsong": ".whichsong`\
\nUSE - Reply to a song file, to recognise the song."})