from telethon import events
import asyncio
from astro.utils import admin_cmd

@astro.on(admin_cmd(pattern="roket ?(.*)"))
async def roket(event):
    ANIMASYON = ["⠀⠀⠀⠀⠀⠀.　　　　　　　　　　⠀⠀⠀✦\n ⠀ ⠀　　　　　　　　　　　　　　⠀⠀⠀⠀⠀* ⠀⠀⠀.　　　　　　　　　　.\r ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀✦\n⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀☄️ ⠀ ⠀⠀⠀⠀⠀⠀.\n⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀☀️\n　　　　　　*　　　　　　　　　　　 　　　　　⠀　　　　⠀　　,⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀.\n　　　　 　　⠀　　　⠀.　 　　˚　　　⠀　⠀  　　,　　　　　　　　　　　　*⠀　　⠀  　　　　　⠀✦⠀\n　　　　　　　　　　　　　　　　　　　.\n　　　　.　　　　.　　　⠀🌕\n　　　.　　　　🚀　　　˚　　　　　　　　ﾟ 　　　.\n.⠀　　🌎⠀‍⠀‍⠀‍⠀‍⠀‍⠀‍⠀‍⠀‍⠀‍⠀‍⠀,　　　*　　⠀.\n　　　　　.　　　　　　　　　　⠀✦\n　˚　　　　　　　　　　　　　　*\n.⠀ 　　　　　　　　　　.　　　　　　　　.\n　　　　　✦⠀　   　　　,　　    　　　　　　　　."] 
    await event.edit("...")
    for anim in ANIMASYON:
        await event.edit(anim)
        await asyncio.sleep(0.2)