#.Astro
# thanks to ult

from telethon.tl.functions.channels import GetFullChannelRequest as getchat
from telethon.tl.functions.phone import CreateGroupCallRequest as startvc
from telethon.tl.functions.phone import DiscardGroupCallRequest as stopvc
from telethon.tl.functions.phone import EditGroupCallTitleRequest as settitle
from telethon.tl.functions.phone import GetGroupCallRequest as getvc
from telethon.tl.functions.phone import InviteToGroupCallRequest as invitetovc
from astro.utils import admin_cmd 
from astro.helps.edef import eor
from astro import CMD_HELP


async def get_call(event):
    mm = await event.client(getchat(event.chat_id))
    xx = await event.client(getvc(mm.full_chat.call, limit=1))
    return xx.call

def user_list(l, n):
    for i in range(0, len(l), n):
        yield l[i : i + n]


@astro.on(admin_cmd(pattern="stopvc$"))
async def _(e):
  if e.is_private==False:
    try:
        await e.client(stopvc(await get_call(e)))
        await e.eor("Oki!\n`VoiceChat is Stopped!")
    except Exception as ex:
        await e.eor(f"#Error↓\n`{ex}`")
  else:
    await e.edit("This Works in Group!🥲💔")

@astro.on(admin_cmd(pattern="vcinvite$"))
async def _(e):
  if e.is_private==False:
    ok = await e.eor("inviting....")
    users = []
    z = 0
    async for x in e.client.iter_participants(e.chat_id):
        if not x.bot:
            users.append(x.id)
    hmm = list(user_list(users, 6))
    for p in hmm:
        try:
            await e.client(invitetovc(call=await get_call(e), users=p))
            z += 6
        except BaseException:
            pass
    await ok.edit("Successfully invited {} to VoiceChat by Astro🛰️").format(z)
  else: 
      await e.edit("This Works in Group🥲💔")

@astro.on(admin_cmd(pattern="startvc$"))
async def _(e):
  if e.is_private==False:
    try:
        await e.client(startvc(e.chat_id))
        await e.eor("Yes!\n`Voice Chat id Started...`")
    except Exception as ex:
        await e.eor(f"#Error↓\n`{ex}`")
  else: 
      await e.edit("This Works in Group🥲💔")


@astro.on(admin_cmd(pattern="vctitle(?: |$)(.*)"))
async def _(e):
  if e.is_private==False:
    title = e.pattern_match.group(1)
    if not title:
        return await e.eor("Changing Vc Title")
    try:
        await e.client(settitle(call=await get_call(e), title=title.strip()))
        await e.eor("Sucessfully Changed title to {}".format(title))
    except Exception as ex:
        await e.eor(f"#Error↓\n`{ex}`")
  else: 
      await e.edit("This Works in Group🥲💔")

CMD_HELP.update({"vctools": ".startvc`\
\nStart Group Call in a group.\
\n\n.stopvc`\
\nStop Group Call in a group.\
\n\n.vctitle <title>`\
\nChange the title Group call.\
\n\n.vcinvite`\
\nInvite all members of group in Group CCall\
    (You must be joined)"
})
