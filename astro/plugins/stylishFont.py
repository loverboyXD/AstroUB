from astro.utils import admin_cmd

normiefont = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
]
weebyfont = [
    "卂",
    "乃",
    "匚",
    "刀",
    "乇",
    "下",
    "厶",
    "卄",
    "工",
    "丁",
    "长",
    "乚",
    "从",
    "𠘨",
    "口",
    "尸",
    "㔿",
    "尺",
    "丂",
    "丅",
    "凵",
    "リ",
    "山",
    "乂",
    "丫",
    "乙",
]

capsi = [
  "ᴀ",
  "ʙ",
  "ᴄ",
  "ᴅ",
  "ᴇ",
  "ғ",
  "ɢ",
  "ʜ",
  "ɪ",
  "ᴊ",
  "ᴋ",
  "ʟ",
  "ᴍ",
  "ɴ",
  "ᴏ",
  "ᴘ",
  "ǫ",
  "ʀ",
  "s",
  "ᴛ",
  "ᴜ",
  "ᴠ",
  "ᴡ",
  "x",
  "ʏ",
  "ᴢ",
]

latin = [
"𝐴",
"𝐵",
"𝐶",
"𝐷",
"𝐸",
"𝐹",
"𝐺",
"𝐻",
"𝐼",
"𝐽",
"𝐾",
"𝐿",
"𝑀",
"𝑁",
"𝑂",
"𝑃",
"𝑄",
"𝑅",
"𝑆",
"𝑇",
"𝑈",
"𝑉",
"𝑊",
"𝑋",
"𝑌",
"𝑍",
]

bold = [
"𝗔",
"𝗕",
"𝗖",
"𝗗",
"𝗘",
"𝗙",
"𝗚",
"𝗛",
"𝗜",
"𝗝",
"𝗞",
"𝗟",
"𝗠",
"𝗡",
"𝗢",
"𝗣",
"𝗤",
"𝗥",
"𝗦",
"𝗧",
"𝗨",
"𝗩",
"𝗪",
"𝗫",
"𝗬",
"𝗭",
]

itcbold = [
"𝘼",
"𝘽",
"𝘾",
"𝘿",
"𝙀",
"𝙁",
"𝙂",
"𝙃",
"𝙄",
"𝙅",
"𝙆",
"𝙇",
"𝙈",
"𝙉",
"𝙊",
"𝙋",
"𝙌",
"𝙍",
"𝙎",
"𝙏",
"𝙐",
"𝙑",
"𝙒",
"𝙓",
"𝙔",
"𝙕",
]

royal = [
"𝐀",
"𝐁",
"𝐂",
"𝐃",
"𝐄",
"𝐅",
"𝐆",
"𝐇",
"𝐈",
"𝐉",
"𝐊",
"𝐋",
"𝐌",
"𝐍",
"𝐎",
"𝐏",
"𝐐",
"𝐑",
"𝐒",
"𝐓",
"𝐔",
"𝐕",
"𝐖",
"𝐗",
"𝐘",
"𝐙",
]

itcroyal = [
"𝑨",
"𝑩",
"𝑪",
"𝑫",
"𝑬",
"𝑭",
"𝑮",
"𝑯",
"𝑰",
"𝑱",
"𝑲",
"𝑳",
"𝑴",
"𝑵",
"𝑶",
"𝑷",
"𝑸",
"𝑹",
"𝑺",
"𝑻",
"𝑼",
"𝑽",
"𝑾",
"𝑿",
"𝒀",
"𝒁",
]

small = [
"ᵃ",
"ᵇ",
"ᶜ",
"ᵈ",
"ᵉ",
"ᶠ",
"ᵍ",
"ʰ",
"ⁱ",
"ʲ",
"ᵏ",
"ˡ",
"ᵐ",
"ⁿ",
"ᵒ",
"ᵖ",
"ᵠ",
"ʳ",
"ˢ",
"ᵗ",
"ᵘ",
"ᵛ",
"ʷ",
"ˣ",
"ʸ",
"ᶻ",
]

@borg.on(admin_cmd(pattern="weeb ?(.*)"))
async def weebify(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = weebyfont[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)
  
@borg.on(admin_cmd(pattern="caps ?(.*)"))
async def caps(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = capsi[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)


@borg.on(admin_cmd(pattern="latin ?(.*)"))
async def weebify(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = latin[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)


@borg.on(admin_cmd(pattern="bold ?(.*)"))
async def weebify(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = bold[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)


@borg.on(admin_cmd(pattern="royal ?(.*)"))
async def weebify(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = royal[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)

@borg.on(admin_cmd(pattern="itcbold ?(.*)"))
async def weebify(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = itcbold[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)


@borg.on(admin_cmd(pattern="small ?(.*)"))
async def weebify(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = small[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)
  
@borg.on(admin_cmd(pattern="royalitc ?(.*)"))
async def weebify(event):

    args = event.pattern_match.group(1)
    if not args:
        get = await event.get_reply_message()
        args = get.text
    if not args:
        await event.edit("`What I am Supposed to Weebify U Dumb`")
        return
    string = "  ".join(args).lower()
    for normiecharacter in string:
        if normiecharacter in normiefont:
            weebycharacter = itcroyal[normiefont.index(normiecharacter)]
            string = string.replace(normiecharacter, weebycharacter)
    await event.edit(string)

