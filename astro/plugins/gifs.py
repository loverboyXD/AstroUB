# Astro-UB
# Astro-Dev
# ©Alone_loverboy

from telethon import events
import asyncio
from astro.utils import admin_cmd
import requests
from astro import CMD_HELP

@astro.on(admin_cmd(pattern="gif ?(.*)"))
async def gif(e):
    arg = e.pattern_match.group(1)
    if arg == "hugs":
        args = "hug"
    elif arg == "blink":
        args = "wink"
    elif arg == "pat":
        args = "pat"        
    else:
        arg = "hugs"
        args = "hug"

    gif = requests.get(f'https://some-random-api.ml/animu/{args}').json()
    await e.delete()
    await e.client.send_message(
        e.chat_id,
        f"Take a  {arg} GIF",
        file=gif["link"]
    )
CMD_HELP.update({
    "gif":
    ".gif hugs\
\nuse: Randomly throws a hug GIF.\
\n\n.gif blink\
\nuse: Throws a random Blink GIF.\
\n\n.gif pat\
\nuse:Randomly throws a head patting GIF ."
})