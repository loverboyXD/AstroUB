
import os
import time
from datetime import datetime as dt
from astro import CMD_HELP
from astro.helps.defines import metadata, bash, downloader, uploader
from astro.helps.defines import time_formatter, mediainfo
from astro.helps.defines import duration_s, genss, stdr 
from astro.utils import humanbytes, admin_cmd 
from telethon.tl.types import DocumentAttributeAudio



@astro.on(admin_cmd(pattern="makevoice$"))
async def vnc(e):
    if not e.reply_to:
        return await eor(e, "Reply to Audio File..My Pro Master🥲💔")
    r = await e.get_reply_message()
    if not mediainfo(r.media).startswith(("audio", "video")):
        return await eor(e, "Provide media Mr. Pro🥲")
    xxx = await eor(e, "Processing..")
    dl = r.file.name
    c_time = time.time()
    file = await downloader(
        "resources/downloads/" + dl, r.media.document, xxx, c_time, "Downloading " + dl + "...")
    await xxx.edit("Done!...\nFetching Data")
    await bash(f"ffmpeg -i '{file.name}' -map 0:a -codec:a libopus -b:a 100k -vbr on out.opus")
    await e.client.send_message(
        e.chat_id, file="out.opus", force_document=False, reply_to=r
    )
    await xxx.delete()
    os.remove(file.name)
    os.remove("out.opus")


@astro.on(admin_cmd(pattern="atrim ?(.*)"))
async def trim_aud(e):
    sec = e.pattern_match.group(1)
    if not sec or "-" not in sec:
        return await eor(e, "Error Reply to Aduio file")
    a, b = sec.split("-")
    if int(a) >= int(b):
        return await eor(e, "Error reply to media")
    vido = await e.get_reply_message()
    if vido and vido.media and mediainfo(vido.media).startswith(("video", "audio")):
        if hasattr(vido.media, "document"):
            vfile = vido.media.document
            name = vido.file.name
        else:
            vfile = vido.media
            name = ""
        if not name:
            name = dt.now().isoformat("_", "seconds") + ".mp4"
        xxx = await eor(e, "Downloading..")
        c_time = time.time()
        file = await downloader("resources/downloads/" + name, vfile, xxx, c_time, "Downloading " + name + "...")
        o_size = os.path.getsize(file.name)
        d_time = time.time()
        diff = time_formatter((d_time - c_time) * 1000)
        file_name = (file.name).split("/")[-1]
        out = file_name.replace(file_name.split(".")[-1], "_trimmed.aac")
        if (b) > (genss(file.name)):            
            os.remove(file.name)
            return await eor(xxx, "Error")
        ss, dd = stdr(int(a)), stdr(int(b))
        xxx = await xxx.edit(
            f"Downloaded `{file.name}` of `{humanbytes(o_size)}` in `{diff}`.\n\nNow Trimming Audio from `{ss}` to `{dd}`..."
        )
        cmd = f'ffmpeg -i "{file.name}" -preset ultrafast -ss {ss} -to {dd} -codec copy -map 0 "{out}" -y'
        await bash(cmd)
        os.remove(file.name)
        f_time = time.time()
        mmmm = await uploader(out, out, f_time, xxx, "Uploading " + out + "...")
        data = await metadata(out)
        artist = 'unknown'
        duration = data["duration"]
        attributes = [
            DocumentAttributeAudio(
                duration=duration,
                title=out.split(".")[0],
                performer=vido.file.performer or artist,
            )
        ]

        caption = "Done!★Download.! in {} Sec. in {}★".format(ss, dd)
        await e.client.send_file(
            e.chat_id,
            mmmm,
            thumb="resources/vAstro.jpg",
            caption=caption,
            attributes=attributes,
            force_document=False,
            reply_to=e.reply_to_msg_id,
        )
        await xxx.delete()
    else:
        await eor(e, "Reply to Media Mr. Pro💔")


@astro.on(admin_cmd(pattern="extractaudio$"))
async def ex_aud(e):
    reply = await e.get_reply_message()
    if not (reply and reply.media and mediainfo(reply.media).startswith("video")):
        return await eor(e, "Error To Video")
    name = reply.file.name or "video.mp4"
    vfile = reply.media.document
    msg = await eor(e, "Downloading..")
    c_time = time.time()
    file = await downloader(
        "resources/downloads/" + name,
        vfile,
        msg,
        c_time,
        "Downloading " + name + "...",
    )
    out_file = file.name + ".aac"
    cmd = f"ffmpeg -i {file.name} -vn -acodec copy {out_file}"
    o, err = await bash(cmd)
    os.remove(file.name)
    data = await metadata(out_file)
    artist = "unknown"
    duration = data["duration"]
    attributes = [
        DocumentAttributeAudio(
            duration=reply.file.duration or duration,
            title=reply.file.name.split(".")[0]
            if reply.file.name
            else "Extracted Audio",
            performer=reply.file.performer or artist,
        )
    ]

    f_time = time.time()
    try:
        fo = await uploader(
            out_file,
            out_file,
            f_time,
            msg,
            "Uploading " + out_file + "...",
        )
    except FileNotFoundError:
        return await eor(msg, "Error Wrong File!")
    await e.client.send_file(
        e.chat_id,
        fo,
        caption="Your Audio File Saar",
        thumb="resources/vAstro.jpg",
        attributes=attributes,
        reply_to=e.reply_to_msg_id,
    )
    await msg.delete()
    
CMD_HELP.update({"audiotools": ".makevoice <reply to audio>`\
\nUSE - creates a voice note from Audio.\
\n\n.extractaudio <reply to media>`\
\nUSE - To extract the audio from it.\
\n\n.atrim <from time> - <to time>`\
trim audio as per given time.\
time must be in seconds.\nEx: .atrim 50-70"
})
