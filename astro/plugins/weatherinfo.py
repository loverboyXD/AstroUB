"""Get weather data using OpenWeatherMap
Syntax: .weather <Location>
.wttr <location> """

import io
import time

import aiohttp

from astro.utils import admin_cmd
from astro import CMD_HELP

@astro.on(admin_cmd(pattern="weather (.*)"))
@astro.on(sudo_cmd(pattern="weather (.*)", allow_sudo=True))
async def _(event):
    if event.fwd_from:
        return
    Key = "f806cebbd44f34cc4cd1d79a290081be"
    sample_url = (
        "https://api.openweathermap.org/data/2.5/weather?q={}&appid={}"
    )
    input_str = event.pattern_match.group(1)
    async with aiohttp.ClientSession() as session:
        response_api_zero = await session.get(
            sample_url.format(input_str, Key)
        )
    response_api = await response_api_zero.json()
    if response_api["cod"] == 200:
        country_code = response_api["sys"]["country"]
        country_time_zone = int(response_api["timezone"])
        sun_rise_time = int(response_api["sys"]["sunrise"]) + country_time_zone
        sun_set_time = int(response_api["sys"]["sunset"]) + country_time_zone
        await eor(
            event,
            """{}
**Temperature**: {}°С
    __minimium__: {}°С
    __maximum__ : {}°С
**Humidity**: {}%
**wind**: {}m/s
**clouds**: {}hpa
**Sunrise**: {} {}
**Sunset**: {} {}""".format(
                input_str,
                response_api["main"]["temp"],
                response_api["main"]["temp_min"],
                response_api["main"]["temp_max"],
                response_api["main"]["humidity"],
                response_api["wind"]["speed"],
                response_api["clouds"]["all"],
                # response_api["main"]["pressure"],
                time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(sun_rise_time)),
                country_code,
                time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(sun_set_time)),
                country_code,
            ),
        )
    else:
        await eor(event, response_api["message"])

CMD_HELP.update({"weatherinfo": ".weather <city name>\
\n use- Get the Current wreather information of the City"
})
