import io
import os
import random
import re
from os import remove
import json 
from astro import bot, astro
import cv2
import numpy as np
import requests
from telethon.tl.types import InputStickerSetID
from PIL import Image
from telethon.tl.functions.messages import GetStickerSetRequest
from telethon.utils import pack_bot_file_id
from telethon.tl import functions, types
from telethon.errors import YouBlockedUserError
from telethon.tl.functions.stickers import SuggestShortNameRequest
from telethon.tl.types import DocumentAttributeFilename, DocumentAttributeSticker
from telethon.utils import get_input_document
from astro.utils import admin_cmd
from astro.helps.defines import deEmojify, bash
from astro.helps.edef import eor
from astro import LOGS, CMD_HELP 
from astro.plugins import con, quotly
from astro.helps.asyn import async_searcher
from astro.helps.defines import resize_photo_sticker

KANGING_STR = [
    "Using Witchery to kang this sticker...",
    "Plagiarising hehe...",
    "Inviting this sticker over to my pack...",
    "Kanging this sticker...",
    "Hey that's a nice sticker!\nMind if I kang?!..",
    "Hehe me stel ur stiker...",
    "Ay look over there (☉｡☉)!→\nWhile I kang this...",
    "Roses are red violets are blue, kanging this sticker so my pack looks cool",
    "Imprisoning this sticker...",
    "Mr.Steal-Your-Sticker is stealing this sticker... ",
]


@astro.on(admin_cmd(pattern="waifu(?:\s|$)([\s\S]*)"))
async def waifu(event):
    text = event.pattern_match.group(1)
    if not text:
        if event.is_reply:
            text = (await event.get_reply_message()).message
        else:
            await eor(event, "Give some text... **PRO !!**")
            return
    animus = [1, 3, 7, 9, 13, 22, 34, 35, 36, 37, 43, 44, 45, 52, 53, 55]
    sticcers = await event.client.inline_query(
        "stickerizerbot", f"#{random.choice(animus)}{(deEmojify(text))}"
    ) 
    await sticcers[0].click(
        event.chat_id,
        reply_to=event.reply_to_msg_id,
        silent=True if event.is_reply else False,
        hide_via=True,
    )
    await event.delete()

@astro.on(admin_cmd(pattern="stkrid"))
async def _(event):
    if event.reply_to_msg_id:
        await event.get_input_chat()
        r_msg = await event.get_reply_message()
        if r_msg.media:
            bot_api_file_id = pack_bot_file_id(r_msg.media)
            await eor(
                event,
                "\n**From User ID:**  `{}`\n**Bot API File ID:**  `{}`".format(
                    str(r_msg.sender_id),
                    bot_api_file_id,
                    str(r_msg.id),
                ),
            )
        else:
            await eor(event, "Reply to Sticker Mr. Pro🤦")

@astro.on(admin_cmd(pattern="stkrinfo$"))
async def get_pack_info(event):
    if not event.is_reply:
        await eor(event, "`I can't fetch info from black hole!!!`")
        return
    rep_msg = await event.get_reply_message()
    if not rep_msg.document:
        await eor(event, "`Reply to a sticker to get the pack details`")
        return
    try:
        stickerset_attr = rep_msg.document.attributes[1]
        event = await eor(event, "`Fetching details of the sticker pack, please wait..`")
    except BaseException:
        await eor(event, "`This is not a sticker. Reply to a sticker.`")
        return

    if not isinstance(stickerset_attr, DocumentAttributeSticker):
        await event.edit("`This is not a sticker. Reply to a sticker.`")
        return

    get_stickerset = await event.client(
        GetStickerSetRequest(
            InputStickerSetID(
                id=stickerset_attr.stickerset.id,
                access_hash=stickerset_attr.stickerset.access_hash,
            )
        )
    )
    pack_emojis = []
    for document_sticker in get_stickerset.packs:
        if document_sticker.emoticon not in pack_emojis:
            pack_emojis.append(document_sticker.emoticon)

    OUTPUT = (
        f"• **Sticker Title :** `{get_stickerset.set.title}\n`"
        f"• **Sticker Short Name :** `{get_stickerset.set.short_name}`\n"
        f"• **Official :** `{get_stickerset.set.official}`\n"
        f"• **Archived :** `{get_stickerset.set.archived}`\n"
        f"• **Stickers In Pack :** `{len(get_stickerset.packs)}`\n"
        f"• **Emojis In Pack :**\n{' '.join(pack_emojis)}"
    )

    await event.edit(OUTPUT)



@astro.on(admin_cmd(pattern="tiny$"))
async def ultiny(event):
    reply = await event.get_reply_message()
    if not (reply and (reply.media)):
        await eor(event, "Reply to Sticker💔 Mr. Pro")
        return
    xx = await eor(event, "__Processing__")
    ik = await event.client.download_media(reply)
    im1 = Image.open("resources//astro_blnk.png")
    if ik.endswith(".tgs"):
        await event.client.download_media(reply, "ast.tgs")
        await bash("lottie_convert.py ast.tgs json.json")
        with open("json.json") as json:
            jsn = json.read()
        jsn = jsn.replace("512", "2000")
        open("json.json", "w").write(jsn)
        await bash("lottie_convert.py json.json ast.tgs")
        file = "ast.tgs"
        os.remove("json.json")
    elif ik.endswith((".gif", ".mp4")):
        iik = cv2.VideoCapture(ik)
        dani, busy = iik.read()
        cv2.imwrite("i.png", busy)
        fil = "i.png"
        im = Image.open(fil)
        z, d = im.size
        if z == d:
            xxx, yyy = 200, 200
        else:
            t = z + d
            a = z / t
            b = d / t
            aa = (a * 100) - 50
            bb = (b * 100) - 50
            xxx = 200 + 5 * aa
            yyy = 200 + 5 * bb
        k = im.resize((int(xxx), int(yyy)))
        k.save("k.png", format="PNG", optimize=True)
        im2 = Image.open("k.png")
        back_im = im1.copy()
        back_im.paste(im2, (150, 0))
        back_im.save("o.webp", "PNG", quality=95)
        file = "o.webp"
        os.remove(fil)
        os.remove("k.png")
    else:
        im = Image.open(ik)
        z, d = im.size
        if z == d:
            xxx, yyy = 200, 200
        else:
            t = z + d
            a = z / t
            b = d / t
            aa = (a * 100) - 50
            bb = (b * 100) - 50
            xxx = 200 + 5 * aa
            yyy = 200 + 5 * bb
        k = im.resize((int(xxx), int(yyy)))
        k.save("k.png", format="PNG", optimize=True)
        im2 = Image.open("k.png")
        back_im = im1.copy()
        back_im.paste(im2, (150, 0))
        back_im.save("o.webp", "WEBP", quality=95)
        file = "o.webp"
        os.remove("k.png")
    await event.client.send_file(event.chat_id, file, reply_to=event.reply_to_msg_id)
    await xx.delete()
    os.remove(file)
    os.remove(ik)


@astro.on(admin_cmd(pattern="packkang"))
async def pack_kangish(_):
    _e = await _.get_reply_message()
    user = astro.me

    if not _e:
        return await eor(_, "Give a Pack")
    if len(_.text) > 9:
        _packname = _.text.split(" ", maxsplit=1)[1]
    else:
        _packname = f"{user.first_name} Pack"
    _id = _e.media.document.attributes[1].stickerset.id
    _hash = _e.media.document.attributes[1].stickerset.access_hash
    _get_stiks = await _.client(
        functions.messages.GetStickerSetRequest(
            stickerset=types.InputStickerSetID(id=_id, access_hash=_hash)
        )
    )
    stiks = []
    for i in _get_stiks.documents:
        x = get_input_document(i)
        stiks.append(
            types.InputStickerSetItem(
                document=x,
                emoji=(i.attributes[1]).alt,
            )
        )
    try:
        short_name = (await _.client(SuggestShortNameRequest(_packname))).short_name
        _r_e_s = await bot(
            functions.stickers.CreateStickerSetRequest(
                user_id=_.sender_id,
                title=_packname,
                short_name=f"u{short_name}_by_{bot.me.username}",
                stickers=stiks,
            )
        )
    except BaseException as er:
        LOGS.exception(er)
        return await eor(_, str(er))
    await eor(
        _, f"__Sucessfully Kanged__\n🛰️\nPack Can be Found [HERE🛸](https://t.me/addstickers/{_r_e_s.set.short_name})")

@astro.on(admin_cmd(pattern="kang"))
async def hehe(args):
    astro = args.client
    xx = await args.eor("Processing...")
    user = astro.me
    username = user.username
    if not username:
        username = user.first_name
    else:
        username = "@" + username
    message = await args.get_reply_message()
    photo = None
    is_anim, is_vid = False, False
    emoji = None
    if not message:
        return await xx.eor("Please Reply to Sticker Mr. piro💔🤎")
    if message.photo:
        photo = io.BytesIO()
        photo = await astro.download_media(message.photo, photo)
    elif message.file and "image" in message.file.mime_type.split("/"):
        photo = io.BytesIO()
        await astro.download_file(message.media.document, photo)
        if (
            DocumentAttributeFilename(file_name="sticker.webp")
            in message.media.document.attributes
        ):
            emoji = message.media.document.attributes[1].alt

    elif message.file and "video" in message.file.mime_type.split("/"):
        xy = await message.download_media()
        if (message.file.duration or 0) <= 10:
            is_vid = True
            photo = await con.create_webm(xy)   # immport
        else:
            y = cv2.VideoCapture(xy)
            heh, lol = y.read()
            cv2.imwrite("ult.webp", lol)
            photo = "ult.webp"
    elif message.file and "tgsticker" in message.file.mime_type:
        await astro.download_file(
            message.media.document,
            "AnimatedSticker.tgs",
        )
        attributes = message.media.document.attributes
        for attribute in attributes:
            if isinstance(attribute, DocumentAttributeSticker):
                emoji = attribute.alt
        is_anim = True
        photo = 1
    elif message.message:
        photo = await quotly.create_quotly(message) # immport
    else:
        return await xx.edit("cannot kang this kang")

    ra = random.choice(KANGING_STR)
    await xx.edit(f"`{ra}`")
    if photo:
        splat = args.text.split()
        pack = 1
        if not emoji:
            emoji = "🏵"
        if len(splat) == 3:
            pack = splat[2]  # User sent astroh
            emoji = splat[1]
        elif len(splat) == 2:
            if splat[1].isnumeric():
                pack = int(splat[1])
            else:
                emoji = splat[1]

        packname = f"ast_{user.id}_{pack}"
        packnick = f"{username}'s ƛsτʀ๏ Pack {pack}"
        cmd = "/newpack"
        file = io.BytesIO()

        if is_vid:
            packname += "_vid"
            packnick += " (Video)"
            cmd = "/newvideo"
        elif is_anim:
            packname += "_anim"
            packnick += " (Animated)"
            cmd = "/newanimated"
        else:
            image = resize_photo_sticker(photo)
            file.name = "sticker.png"
            image.save(file, "PNG")

        response = await async_searcher(f"http://t.me/addstickers/{packname}")
        htmlstr = response.split("\n")

        if (
            "  A <strong>Telegram</strong> user has created the <strong>Sticker&nbsp;Set</strong>."
            not in htmlstr
        ):
            async with astro.conversation("@Stickers") as conv:
                try:
                    await conv.send_message("/addsticker")
                except YouBlockedUserError:
                    LOGS.info("Unblocking @Stickers for kang...")
                    await astro(functions.contacts.UnblockRequest("stickers"))
                    await conv.send_message("/addsticker")
                await conv.get_response()
                await conv.send_message(packname)
                x = await conv.get_response()
                if x.text.startswith("Alright! Now send me the video sticker."):
                    await conv.send_file(photo, force_document=True)
                    x = await conv.get_response()
                t = "50" if (is_anim or is_vid) else "120"
                while t in x.message:
                    pack += 1
                    packname = f"ult_{user.id}_{pack}"
                    packnick = f"{username}'s Pack {pack}"
                    if is_anim:
                        packname += "_anim"
                        packnick += " (Animated)"
                    elif is_vid:
                        packnick += " (Video)"
                        packname += "_vid"
                    await xx.edit("__Switching to Pack of {} \n due to insufficient space__".format(pack))
                    await conv.send_message("/addsticker")
                    await conv.get_response()
                    await conv.send_message(packname)
                    x = await conv.get_response()
                    if x.text.startswith("Alright! Now send me the video sticker."):
                        await conv.send_file(photo, force_document=True)
                        x = await conv.get_response()
                    if x.text in ["Invalid pack selected.", "Invalid set selected."]:
                        await conv.send_message(cmd)
                        await conv.get_response()
                        await conv.send_message(packnick)
                        await conv.get_response()
                        if is_anim:
                            await conv.send_file("AnimatedSticker.tgs")
                            remove("AnimatedSticker.tgs")
                        else:
                            if is_vid:
                                file = photo
                            else:
                                file.seek(0)
                            await conv.send_file(file, force_document=True)
                        await conv.get_response()
                        await conv.send_message(emoji)
                        await conv.get_response()
                        await conv.send_message("/publish")
                        if is_anim:
                            await conv.get_response()
                            await conv.send_message(f"<{packnick}>")
                        await conv.get_response()
                        await conv.send_message("/skip")
                        await conv.get_response()
                        await conv.send_message(packname)
                        await conv.get_response()
                        await xx.edit(f"`Sticker added in a Different Pack !\
                            \nThis Pack is Newly created!\
                            \nYour pack can be found [🛸HERE🍂](t.me/addstickers/{packname})",
                            parse_mode="md",
                        )
                        return
                if is_anim:
                    await conv.send_file("AnimatedSticker.tgs")
                    remove("AnimatedSticker.tgs")
                elif "send me an emoji" not in x.message:
                    if is_vid:
                        file = photo
                    else:
                        file.seek(0)
                    await conv.send_file(file, force_document=True)
                    rsp = await conv.get_response()
                    if "Sorry, the file type is invalid." in rsp.text:
                        await xx.edit("INvalid type of file!!",
                        )
                        return
                await conv.send_message(emoji)
                await conv.get_response()
                await conv.send_message("/done")
                await conv.get_response()
                await astro.send_read_acknowledge(conv.chat_id)
        else:
            await xx.edit("`Brewing a new Pack...`")
            async with astro.conversation("Stickers") as conv:
                await conv.send_message(cmd)
                await conv.get_response()
                await conv.send_message(packnick)
                await conv.get_response()
                if is_anim:
                    await conv.send_file("AnimatedSticker.tgs")
                    remove("AnimatedSticker.tgs")
                else:
                    if is_vid:
                        file = photo
                    else:
                        file.seek(0)
                    await conv.send_file(file, force_document=True)
                rsp = await conv.get_response()
                if "Sorry, the file type is invalid." in rsp.text:
                    await xx.edit("INvalid type of File!!",
                    )
                    return
                await conv.send_message(emoji)
                await conv.get_response()
                await conv.send_message("/publish")
                if is_anim:
                    await conv.get_response()
                    await conv.send_message(f"<{packnick}>")

                await conv.get_response()
                await conv.send_message("/skip")
                await conv.get_response()
                await conv.send_message(packname)
                await conv.get_response()
                await astro.send_read_acknowledge(conv.chat_id)
        await xx.edit("•__Sucessfully Kanged__✨\nEmoji: {} \nPack: [🍃HERE](https://t.me/addstickers/{})".format(emoji, packname),
            parse_mode="md",
        )
        try:
            os.remove(photo)
        except BaseException:
            pass


CMD_HELP.update({"stickers": ".kang <emoji> <number>\
\nUSE- Adds the sticker to desired pack with a custom emoji of your choice. If emoji is not mentioned then default is 🌌. And if number is not mentioned then Pack will go on serial wise. \n  ✓(1 pack = 120 non-animated stickers)\n  ✓(1 pack = 50 animated stickers)\
\n\n.stkrinfo <reply to sticker>\
\nUSE - Gets all the infos of the sticker pack\
\n\n.stkrid <reply to Sticker>\
\nUSE - Get the ID of any sticker\
\n\n.waifu\
\nUSE - send a waifu text sticker\
\n\n.packkang <reply to a sticker> <pack name>\
\nUSE - Kang the Complete sticker set (with custom name).\
\n\n.tiny <reply to media>`\
\nTo create Tiny stickers."
})
