
from telethon.tl.types import ChannelParticipantAdmin as admin
from telethon.tl.types import ChannelParticipantCreator as owner
from telethon.tl.types import UserStatusOffline as off
from telethon.tl.types import UserStatusOnline as onn
from telethon.tl.types import UserStatusRecently as rec 
from telethon.utils import get_display_name

from astro import CMD_HELP, bot
from astro.utils import admin_cmd
from astro.helps.defines import inline_mention

@astro.on(admin_cmd(pattern="tag(on|off|all|bots|rec|admins|owner) ?(.*)"))
async def _(e):
  if e.is_private==False:
    okk = e.text
    lll = e.pattern_match.group(2)
    o = 0
    nn = 0
    rece = 0
    xx = f"{lll}" if lll else ""
    lili = await e.client.get_participants(e.chat_id, limit=99)
    for bb in lili:
        x = bb.status
        y = bb.participant
        if isinstance(x, onn):
            o += 1
            if "on" in okk:
                xx += f"\n{inline_mention(bb)}"
        elif isinstance(x, off):
            nn += 1
            if "off" in okk and not bb.bot and not bb.deleted:
                xx += f"\n{inline_mention(bb)}"
        elif isinstance(x, rec):
            rece += 1
            if "rec" in okk and not bb.bot and not bb.deleted:
                xx += f"\n{inline_mention(bb)}"
        if isinstance(y, owner):
            xx += f"\n꧁{inline_mention(bb)}꧂"
        if isinstance(y, admin) and "admin" in okk and not bb.deleted:
            xx += f"\n{inline_mention(bb)}"
        if "all" in okk and not bb.bot and not bb.deleted:
            xx += f"\n{inline_mention(bb)}"
        if "bot" in okk and bb.bot:
            xx += f"\n{inline_mention(bb)}"
    await e.eor(xx)
  else: 
    await bot.send_message(event.chat_id, "Use in Group burhhh🤷")

CMD_HELP.update({"tags": ".tagall\
    \nTag Top 100 Members of chat.\
    \n\n.tagadmins`\
    \nTag Admins of that chat.\
    \n\n.tagowner`\
    \nTag Owner of that chat\
    \n\n.tagbots`\
    \nTag Bots of that chat.\
    \n\n.tagrec`\
    \nTag recently Active Members.\
    \n\n.tagon`\
    \nTag online Members(work only if privacy off).\
    \n\n.tagoff`\
    \nTag Offline Members(work only if privacy off)."
  
})
