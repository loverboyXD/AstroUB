# Astro-UB
# © AstroUB
from astro.utils import admin_cmd

@astro.on(admin_cmd(pattern="hi ?(.*)"))
async def hi(event):
    giveVar = event.text
    ast = giveVar[4:5]
    if not ast:
        ast = "💮"
    await edit_or_reply(
        event,
        f"{ast}✨✨{ast}✨{ast}{ast}{ast}\n{ast}✨✨{ast}✨✨{ast}✨\n{ast}{ast}{ast}{ast}✨✨{ast}✨\n{ast}✨✨{ast}✨✨{ast}✨\n{ast}✨✨{ast}✨{ast}{ast}{ast}\n☁☁☁☁☁☁☁☁",
    )