import os
from astro.utils import admin_cmd
from astro import CMD_HELP

os.system("pip install pyjokes")
import pyjokes 
My_joke=pyjokes.get_joke(language="en", category="all") 


@astro.on(admin_cmd(pattern="joke ?(.*)"))
async def joke(r):
        await r.edit(My_joke)


CMD_HELP.update({"joke": ".joke - **Use:** Get a Random Joke. IN English"

})