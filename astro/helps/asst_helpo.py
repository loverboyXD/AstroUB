from datetime import datetime
from math import floor

from telethon.utils import get_display_name
from telethon.tl.functions.messages import ImportChatInviteRequest as Get
from telethon.tl.types import MessageEntityMentionName

from astro.plugins.assistant.sql.blacklist_sql import add_user_to_bl, rem_user_from_bl
from astro.config import Config

botusername = Config.BOT_USERNAME

async def reply_id(event):
    reply_to_id = None
    if event.sender_id in Config.SUDO_USERS:
        reply_to_id = event.id
    if event.reply_to_msg_id:
        reply_to_id = event.reply_to_msg_id
    return reply_to_id
    
    
async def ban_user_from_bot(user, reason, reply_to=None):
    try:
        date = str(datetime.now().strftime("%B %d, %Y"))
        add_user_to_bl(event.chat_id)
    except Exception as e:
        LOGS.error(str(e))
    banned_msg = (
        f"**You have been Banned Forever from using this bot.\nReason** : {reason}"
    )
    await tgbot.send_message(event.chat_id, banned_msg)

async def unban_user_from_bot(user, reason, reply_to=None):
    try:
        rem_user_from_bl(event.chat_id)
    except Exception as e:
        LOGS.error(str(e))
    banned_msg = "**You have been Unbanned from this bot. From now on you can send messages .**"

    if reason is not None:
        banned_msg += f"\n**Reason:** __{reason}__"
    await tgbot.send_message(event.chat_id, banned_msg)
